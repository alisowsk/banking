CREATE TABLE User (
	id BIGINT NOT NULL AUTO_INCREMENT,
	created_at DATETIME NOT NULL,
	firstname VARCHAR(255) NOT NULL,
	guid BINARY (255) NOT NULL,
	lastname VARCHAR(255) NOT NULL,
	mail VARCHAR(255) NOT NULL,
	title VARCHAR(255) NOT NULL,
	username VARCHAR(255) NOT NULL,
	role VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	version BIGINT NOT NULL,
	enabled bit DEFAULT 1,
	blocked_at DATETIME,
	updated_at DATETIME,
	PRIMARY KEY (id)
);

ALTER TABLE User ADD UNIQUE (guid);

CREATE TABLE Client (
	client_id BIGINT NOT NULL AUTO_INCREMENT,
	created_at DATETIME NOT NULL,
	name VARCHAR(255) NOT NULL,
	surname VARCHAR(255) NOT NULL,
	national_identification_number VARCHAR(255) NOT NULL,
	gender VARCHAR(255) NOT NULL,
	PRIMARY KEY (client_id)
);

CREATE TABLE Currency (
	id BIGINT NOT NULL auto_increment,
   symbol VARCHAR(3) NOT NULL,
	PRIMARY KEY (id)
	);

CREATE TABLE Brand (
	id BIGINT NOT NULL AUTO_INCREMENT,
	brand_name VARCHAR(256) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE Account (
	id BIGINT NOT NULL auto_increment,
	created_at DATETIME NOT NULL,
   balance DOUBLE NOT NULL,
   iban VARCHAR(255) NOT NULL,
   active bit not null DEFAULT 1,
   currency_id BIGINT NOT NULL,
   user_id BIGINT NOT NULL,
	PRIMARY KEY (id),
   FOREIGN KEY (user_id) REFERENCES User(id),
   FOREIGN KEY (currency_id) REFERENCES Currency(id)
	);

CREATE TABLE Transaction (
	id BIGINT NOT NULL auto_increment,
	created_at DATETIME NOT NULL,
	from_iban VARCHAR(255) NOT NULL,
	to_iban VARCHAR(255) NOT NULL,
	currency_id BIGINT NOT NULL,
	value DOUBLE NOT NULL,
	PRIMARY KEY (id),
   FOREIGN KEY (currency_id) REFERENCES Currency(id)
	);

CREATE TABLE Debit_Card (
	card_id BIGINT NOT NULL AUTO_INCREMENT,
	color VARCHAR(7) NOT NULL,
	brand BIGINT NOT NULL,
	daily_limit_transaction_value BIGINT NOT NULL,
	daily_limit_transaction_number BIGINT NOT NULL,
	account BIGINT NOT NULL,
	card_status VARCHAR(10) NOT NULL,
	client BIGINT NOT NULL,
	pan BIGINT NOT NULL,
	expiry_date DATETIME NOT NULL,
	cvv VARCHAR(4) NOT NULL,
	pin VARCHAR(4) NOT NULL,
	is_suspended bit NOT NULL,
	is_blocked bit NOT NULL,
	PRIMARY KEY (card_id)
	/*FOREIGN KEY (brand) REFERENCES Brand(brand_id), TODO*/
	/*FOREIGN KEY (account) REFERENCES Account(account_id), TODO*/
	/*FOREIGN KEY (client) REFERENCES Client(client_id) TODO*/
);

CREATE TABLE Insurance (
	insurance_id BIGINT NOT NULL AUTO_INCREMENT,
	created_at DATETIME NOT NULL,
	duration_time DATETIME NOT NULL,
	type VARCHAR(255) NOT NULL,
	contribution BIGINT NOT NULL,
	PRIMARY KEY (insurance_id)
);

CREATE TABLE Deposit (
	deposit_id BIGINT NOT NULL AUTO_INCREMENT,
	created_at DATETIME NOT NULL,
	const DOUBLE NOT NULL,
	rate BIGINT NOT NULL,
	min_cash DOUBLE NOT NULL,
	max_cash DOUBLE NOT NULL,
	renewable bit NOT NULL,
	capitalization_period BIGINT NOT NULL,
	break_percentage DOUBLE NOT NULL,
	PRIMARY KEY (deposit_id)
);


CREATE TABLE Credit (
	id BIGINT NOT NULL AUTO_INCREMENT,
	created_at DATETIME NOT NULL,
	installment BIGINT NOT NULL,
	provision BIGINT NOT NULL,
	contribution DOUBLE NOT NULL,
   min_value DOUBLE NOT NULL,
   max_value DOUBLE NOT NULL,
	insurance bit NOT NULL,
	active bit NOT NULL,
	changing_rate bit NOT NULL,
	interest BIGINT NOT NULL,
   version BIGINT NOT NULL,
   currency_id BIGINT NOT NULL,
   type VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
	);

CREATE TABLE Installment (
   id BIGINT NOT NULL auto_increment,
   amount DOUBLE NOT NULL,
	created_at DATETIME NOT NULL,
	finish_date DATETIME NOT NULL,
	paied_at DATETIME,
   active bit NOT NULL,
   active_credit_id BIGINT NOT NULL,
   version BIGINT NOT NULL,
	PRIMARY KEY (id)
   );


CREATE TABLE Active_Credit (
  	id bigint NOT NULL auto_increment,
  	credit_id bigint NOT NULL,
  	user_id bigint NOT NULL,
  	value DOUBLE NOT NULL,
  	start_date datetime NOT NULL,
  	finish_date datetime NOT NULL,
   active bit NOT NULL,
  	PRIMARY KEY (id),
  	FOREIGN KEY (credit_id) REFERENCES Credit(id)
  	);

CREATE TABLE Active_Credit_installment (
	credit_id BIGINT NOT NULL,
	installment BIGINT
	);

ALTER TABLE Active_Credit_installment ADD CONSTRAINT  FOREIGN KEY (credit_id) REFERENCES Active_Credit (id);


CREATE TABLE Active_account (
  	active_account_id bigint(20) NOT NULL auto_increment,
  	client_id bigint(20) NOT NULL,
  	account_id bigint(20) NOT NULL,
  	type varchar(60),
  	PRIMARY KEY (active_account_id),
  	FOREIGN KEY (client_id) REFERENCES Client(client_id),
  	FOREIGN KEY (account_id) REFERENCES Account(id)
	);



CREATE TABLE Active_deposit (
  	active_deposit_id bigint(20) NOT NULL,
  	deposit_id bigint(20) NOT NULL,
  	account_id bigint(20) NOT NULL,
  	date_start datetime NOT NULL,
  	PRIMARY KEY (active_deposit_id),
  	FOREIGN KEY (deposit_id) REFERENCES Deposit(deposit_id),
  	FOREIGN KEY (account_id) REFERENCES Account(id)
	); 



CREATE TABLE Active_insurance (
  	active_insurance_id bigint(20) NOT NULL,
  	insurance_id bigint(20) NOT NULL,
  	account_id bigint(20) NOT NULL,
  	date_start datetime NOT NULL,
  	PRIMARY KEY (active_insurance_id),
  	FOREIGN KEY (insurance_id) REFERENCES Insurance(insurance_id),
  	FOREIGN KEY (account_id) REFERENCES Account(id)
	);
