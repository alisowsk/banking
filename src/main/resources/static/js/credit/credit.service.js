(function () {
   'use strict';

   angular
           .module('bank')
           .factory('Credit', Credit);

   Credit.$inject = ['$resource', 'dateResolveUtil'];

   function Credit($resource, dateResolveUtil) {
      return $resource('/api/credit/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         refresh: {
            method: 'POST',
            url: '/api/credit/refresh',
            isArray: false
         },
         logAction: {
            method: 'POST',
            url: '/api/log',
            isArray: false
         },
         finish : {
            method:'PUT',
            url: '/api/credit/finish/:id',
            params:{id : '@id'}
         },
         delete:{
            method:'DELETE',
            url: '/api/credit/delete/:id',
            params:{id : '@id'}
         },
         active: {method: 'GET',
            url: '/api/credit/active',
            isArray: true,
            transformResponse: transformResponse
         }
      });

      ////////////

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(credit) {

         // naprawic daty
//          credit.createdAt = dateResolveUtil.convertToDate(credit.createdAt);
//         credit.startDate = dateResolveUtil.convertToDate(credit.startDate);
//         credit.finishDate = dateResolveUtil.convertToDate(credit.finishDate);
      }
   }
})();
