(function () {
   'use strict';

   angular
           .module('bank')
           .factory('Currency', Currency);

   Currency.$inject = ['$resource', 'dateResolveUtil'];

   function Currency($resource, dateResolveUtil) {
      return $resource('/api/currency/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         refresh: {
            method: 'POST',
            url: '/api/currency/refresh',
            isArray: false
         },
         delete:{
            method:'DELETE',
            url: '/api/currency/delete/:id',
            params:{id : '@id'}
         }
      });

      ////////////

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(currency) {

         // naprawic daty
//          currency.createdAt = dateResolveUtil.convertToDate(currency.createdAt);
//         currency.startDate = dateResolveUtil.convertToDate(currency.startDate);
//         currency.finishDate = dateResolveUtil.convertToDate(currency.finishDate);
      }
   }
})();
