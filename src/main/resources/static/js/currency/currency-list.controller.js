(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CurrencyListCtrl', CurrencyListCtrl);

   CurrencyListCtrl.$inject = [
      '$scope',
      'Currency',
      'currencys',
      '$stateParams',
      '$location',
      'popupService'];

   function CurrencyListCtrl($scope, Currency, currencys, $stateParams, $location, popupService) {
      $scope.loading = false;
      $scope.currencys = currencys;

      $scope.refresh = refresh;
      $scope.deleteCurrency = deleteCurrency;


      function reloadCurrencys() {
         $scope.loading = true;
         Currency.query().$promise
                 .then(function (data) {
                    $scope.currencys = data;
                    $scope.loading = false;
                 });
      }


      function deleteCurrency(currencyId) {
         $scope.loading = true;
         Currency.delete({id: currencyId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    popupService.success('currency-list.popup.delete.success.title', 'currency-list.popup.delete.success.body');
                    for (var i = 0; i < $scope.currencys.length; i++) {
                       if ($scope.currencys[i].currencyId === currencyId) {
                          $scope.currencys.splice(i, 1);
                       }
                    }
                 }, function () {
                    popupService.error('currency-list.popup.delete.failure.title', 'currency-list.popup.delete.failure.body');
                 });
      }

      function refresh() {
         Currency.refresh().$promise
                 .then(function () {
                    reloadCurrencys();
                    $('#ProcessingModal').modal('hide');
                 });
      }
   }
})();