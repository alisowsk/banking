(function () {
   'use strict';

   angular
           .module('bank')
           .config(currencyProvider);

   currencyProvider.$inject = ['$stateProvider'];

   function currencyProvider($stateProvider) {
      var resolveCurrencys = ['$state', 'Currency', 'logToServerUtil', loadCurrencys];
      var resolveSingleCurrency = ['$state', '$stateParams', 'Currency', 'logToServerUtil',
         loadSingleCurrency];

      $stateProvider
              .state('currency', {
                 parent: 'root',
                 url: '/currency',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('currency.list', {
                 url: '/list',
                 reloadOnSearch: false,
                 templateUrl: '/currency/list',
                 controller: 'CurrencyListCtrl',
                 resolve: {
                    currencys: resolveCurrencys,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('currency/list');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('currency.add', {
                 url: '/add',
                 reloadOnSearch: false,
                 templateUrl: '/currency/add',
                 controller: 'CurrencyAddCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('currency/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('currency.edit', {
                 url: '/edit/{id:int}',
                 reloadOnSearch: false,
                 templateUrl: '/currency/edit',
                 controller: 'CurrencyEditCtrl',
                 resolve: {
                    currency: resolveSingleCurrency,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('currency/edit');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('currency.common', {
                 url: '/{id:int}',
                 templateUrl: '/currency/common',
                 controller: 'CurrencyCommonCtrl',
                 redirectTo: 'currency.common.details',
                 resolve: {
                    currency: resolveSingleCurrency,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('currency/common');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('currency.common.details', {
                 url: '/details',
                 templateUrl: '/currency/details',
                 controller: 'CurrencyDetailsCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('currency/details');
                          return $translate.refresh();
                       }]
                 }
              });


      function loadCurrencys($state, Currency, logToServerUtil) {
         var currencysPromise = Currency.query().$promise;
         currencysPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Currencys failed', reason);
//                    $state.go('dashboard');
                 });
         return currencysPromise;
      }

      function loadSingleCurrency($state, $stateParams, Currency, logToServerUtil) {
         var currencyPromise = Currency.get({id: $stateParams.id}).$promise;
         currencyPromise
                 .catch(function (reason) {
                    logToServerUtil.trace('get Currency failed', reason);
                    $state.go('dashboard');
                 });
         return currencyPromise;
      }
   }
})();
