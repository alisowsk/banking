(function () {
   'use strict';

   angular
           .module('bank')
           .controller('MyaccountBoostCtrl', MyaccountBoostCtrl);

   MyaccountBoostCtrl.$inject = [
      '$scope',
      '$state',
      'myaccount',
      'Myaccount',
      'CommonUtilService',
      'logToServerUtil',
      'popupService'];

   function MyaccountBoostCtrl($scope, $state, myaccount, Myaccount, CommonUtilService, logToServerUtil, popupService) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.boostMyaccount = boostMyAccount;
      $scope.myaccount = myaccount;
      $scope.myaccount.balanceTmp = $scope.myaccount.balance;
      $scope.myaccount.balance = '';

      $scope.cancel = cancel;

      function cancel() {
         $state.go('user.common', {id: $state.params.userId});
      }

      function boostMyAccount() {
         var myaccount = new Myaccount();
         myaccount.accountId = $scope.myaccount.accountId;
         myaccount.balance = $scope.myaccount.balance;

         myaccount.$payment()
                 .then(function (result) {
//                    $state.go('myaccount.list');
                    popupService.success('myaccount-boost.popup.save.success.title', 'myaccount-boost.popup.save.success.body');
                    $state.go('user.common', {id: $state.params.userId});
                 }, function (reason) {
                    popupService.error('myaccount-boost.popup.save.failure.title', 'myaccount-boost.popup.save.failure.body');
//                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();