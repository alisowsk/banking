(function () {
   'use strict';

   angular
           .module('bank')
           .controller('MyaccountEditCtrl', MyaccountEditCtrl);

   MyaccountEditCtrl.$inject = [
      '$scope',
      '$state',
//      'myaccount',
      'Myaccount',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function MyaccountEditCtrl($scope, $state/*, myaccount*/, Myaccount, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

//      $scope.myaccount = {
//         myaccountId :myaccount.myaccountId,
//         rateOfInterest : myaccount.rateOfInterest,
//         interest : myaccount.interest,
//         insurance : myaccount.insurance,
//         installment : myaccount.installment,
//         provision : myaccount.provision,
//         finishDate : myaccount.finishDate,
//         startDate : myaccount.startDate
//      };
      $scope.saveMyaccount = saveMyaccount;

      function saveMyaccount() {
         var myaccount = new Myaccount();
         myaccount.rateOfInterest = $scope.myaccount.rateOfInterest;
         myaccount.interest = $scope.myaccount.interest;
         myaccount.insurance = $scope.myaccount.insurance;
         myaccount.installment = $scope.myaccount.installment;
         myaccount.provision = $scope.myaccount.provision;
         myaccount.finishDate = $scope.myaccount.finishDate;
         myaccount.startDate = $scope.myaccount.startDate;
         myaccount.myaccountId = $scope.myaccount.myaccountId;
         myaccount.$update()
                 .then(function (result) {
                    popupService.success('myaccount-edit.popup.save.success.title', 'myaccount-edit.popup.save.success.body');
                    $state.go('myaccount.list');
                 }, function (reason) {
                    popupService.error('myaccount-edit.popup.save.failure.title', 'myaccount-edit.popup.save.failure.body');
                    logToServerUtil.trace('My account Template failure', reason);
                 });
      }
   }
})();