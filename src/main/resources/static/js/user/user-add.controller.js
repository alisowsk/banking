(function () {
   'use strict';

   angular
           .module('bank')
           .controller('UserAddCtrl', UserAddCtrl);

   UserAddCtrl.$inject = [
      '$scope',
      '$state',
      'User',
      'CommonUtilService',
      'logToServerUtil',
      'popupService'];

   function UserAddCtrl($scope, $state, User, CommonUtilService, logToServerUtil, popupService) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.user = {
         firstname: '',
         lastname: '',
         role: 'User'
      };
      $scope.saveUser = saveUser;

      function saveUser() {
         var user = new User();
         user.firstname = $scope.user.firstname;
         user.lastname = $scope.user.lastname;
         user.role = $scope.user.role;

         user.$save()
                 .then(function (result) {
                    popupService.success('user-add.popup.save.success.title', 'user-add.popup.save.success.body');
                    $state.go('user.list');
                 }, function (reason) {
                    popupService.error('user-add.popup.save.failure.title', 'user-add.popup.save.failure.body');
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();