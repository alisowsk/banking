(function () {
   'use strict';

   angular
           .module('bank')
           .config(brandProvider);

   brandProvider.$inject = ['$stateProvider'];

   function brandProvider($stateProvider) {
      var resolveBrands = ['$state', 'Brand', 'logToServerUtil', loadBrands];
      var resolveSingleBrand = ['$state', '$stateParams', 'Brand', 'logToServerUtil',
         loadSingleBrand];

      $stateProvider
              .state('brand', {
                 parent: 'root',
                 url: '/brand',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('brand.list', {
                 url: '/list',
                 reloadOnSearch: false,
                 templateUrl: '/brand/list',
                 controller: 'BrandListCtrl',
                 resolve: {
                    brands: resolveBrands,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('brand/list');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('brand.add', {
                 url: '/add',
                 reloadOnSearch: false,
                 templateUrl: '/brand/add',
                 controller: 'BrandAddCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('brand/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('brand.common', {
                 url: '/{id:int}',
                 templateUrl: '/brand/common',
                 controller: 'BrandCommonCtrl',
                 redirectTo: 'brand.common.details',
                 resolve: {
                    brand: resolveSingleBrand,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('brand/common');
                          return $translate.refresh();
                       }]
                 }
              });


      function loadBrands($state, Brand, logToServerUtil) {
         var brandsPromise = Brand.query().$promise;
         brandsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Brands failed', reason);
                 });
         return brandsPromise;
      }

      function loadSingleBrand($state, $stateParams, Brand, logToServerUtil) {
         var brandPromise = Brand.get({id: $stateParams.id}).$promise;
         brandPromise
                 .catch(function (reason) {
                    logToServerUtil.trace('get Brand failed', reason);
                    $state.go('dashboard');
                 });
         return brandPromise;
      }
   }
})();
