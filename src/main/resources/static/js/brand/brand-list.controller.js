(function () {
   'use strict';

   angular
           .module('bank')
           .controller('BrandListCtrl', BrandListCtrl);

   BrandListCtrl.$inject = [
      '$scope',
      'Brand',
      'brands',
      '$stateParams',
      '$location',
      'popupService'];

   function BrandListCtrl($scope, Brand, brands, $stateParams, $location, popupService) {
      $scope.loading = false;
      $scope.brands = brands;

      $scope.refresh = refresh;
      $scope.deleteBrand = deleteBrand;


      function reloadBrands() {
         $scope.loading = true;
         Brand.query().$promise
                 .then(function (data) {
                    $scope.brands = data;
                    $scope.loading = false;
                 });
      }


      function deleteBrand(brandId) {
         $scope.loading = true;
         Brand.delete({id: brandId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    popupService.success('brand-list.popup.delete.success.title', 'brand-list.popup.delete.success.body');
                    for (var i = 0; i < $scope.brands.length; i++) {
                       if ($scope.brands[i].brandId === brandId) {
                          $scope.brands.splice(i, 1);
                       }
                    }
                 });
      }

      function refresh() {
         Brand.refresh().$promise
                 .then(function () {
                    reloadBrands();
                    $('#ProcessingModal').modal('hide');
                 });
      }
   }
})();