(function () {
   'use strict';

   angular
           .module('bank')
           .controller('BrandCommonCtrl', BrandCommonCtrl);

   BrandCommonCtrl.$inject = ['$scope', 'brand'];

   function BrandCommonCtrl($scope, brand) {
      $scope.brand = brand;
      $scope.changeView = changeView;
      function changeView(state) {
         $scope.view = {
            details: false
         };
         
         switch (state) {
            case 'DETAILS':
               $scope.view.details = true;
               break;
            default:
               break;
         }
      }
   }
})();
