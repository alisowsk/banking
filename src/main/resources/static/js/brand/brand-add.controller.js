(function () {
   'use strict';

   angular
           .module('bank')
           .controller('BrandAddCtrl', BrandAddCtrl);

   BrandAddCtrl.$inject = [
      '$scope',
      '$state',
      'Brand',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function BrandAddCtrl($scope, $state, Brand, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.brand = {
         brandName: ""
      };

      $scope.saveBrand = saveBrand;

      function saveBrand() {
         var brand = new Brand();
         brand.brandName = $scope.brand.brandName;

         brand.$save()
                 .then(function (result) {
                    popupService.success('brand-add.popup.save.success.title', 'brand-add.popup.save.success.body');
                    $state.go('brand.list');
                 }, function (reason) {
                    popupService.error('brand-add.popup.save.failure.title', 'brand-add.popup.save.failure.body');
                    logToServerUtil.trace('Save Brand failure', reason);
                 });
      }
   }
})();