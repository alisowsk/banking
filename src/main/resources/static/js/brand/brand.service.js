(function () {
   'use strict';

   angular
           .module('bank')
           .factory('Brand', Brand);

   Brand.$inject = ['$resource', 'dateResolveUtil'];

   function Brand($resource, dateResolveUtil) {
      return $resource('/api/brand/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         refresh: {
            method: 'POST',
            url: '/api/brand/refresh',
            isArray: false
         },
         delete:{
            method:'DELETE',
            url: '/api/brand/delete/:id',
            params:{id : '@id'}
         }
      });

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(brand) {
      }
   }
})();
