(function () {
   'use strict';

   angular
           .module('bank')
           .controller('ActiveCreditListCtrl', ActiveCreditListCtrl);

   ActiveCreditListCtrl.$inject = [
      '$scope',
      'ActiveCredit',
      'activeCredits',
      '$stateParams',
      '$location',
      'dateResolveUtil',
      'popupService'];

   function ActiveCreditListCtrl($scope, ActiveCredit, activeCredits, $stateParams, $location, dateResolveUtil, popupService) {
      $scope.loading = false;
      $scope.activeCredits = activeCredits;

      $scope.refresh = refresh;
      $scope.finishActiveCredit = finishActiveCredit;
      $scope.deleteActiveCredit = deleteActiveCredit;
      $scope.onlyDate = dateResolveUtil.onlyDate;


      function reloadActiveCredits() {
         $scope.loading = true;
         ActiveCredit.query().$promise
                 .then(function (data) {
                    $scope.activeCredits = data;
                    $scope.loading = false;
                 });
      }

      function deleteActiveCredit(activeCreditId) {
         $scope.loading = true;
         ActiveCredit.delete({id: activeCreditId}).$promise
                 .then(function (data) {
                    popupService.success('activeCredit-list.popup.delete.success.title', 'activeCredit-list.popup.delete.success.body');
                    $scope.loading = false;
                    for (var i = 0; i < $scope.activeCredits.length; i++) {
                       if ($scope.activeCredits[i].activeCreditId === activeCreditId) {
                          $scope.activeCredits.splice(i, 1);
                       }
                    }
                 });
      }

      function finishActiveCredit(activeCreditId) {
         $scope.loading = true;
         ActiveCredit.finish({id: activeCreditId}).$promise
                 .then(function (data) {
                    for (var i = 0; i < $scope.activeCredits.length; i++) {
                       if ($scope.activeCredits[i].activeCreditId === activeCreditId) {
                          $scope.activeCredits[i].active = true;
                       }
                    }
                    popupService.success('activeCredit-list.popup.finish.success.title', 'activeCredit-list.popup.finish.success.body');
                    $scope.loading = false;
                 });
      }

      function refresh() {
         ActiveCredit.refresh().$promise
                 .then(function () {
                    reloadActiveCredits();
                    $('#ProcessingModal').modal('hide');
                 });
      }
   }
})();