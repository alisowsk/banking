(function () {
   'use strict';

   angular
           .module('bank')
           .config(transactionProvider);

   transactionProvider.$inject = ['$stateProvider'];

   function transactionProvider($stateProvider) {
      var resolveTransactions = ['$state', 'Transaction', 'logToServerUtil', loadTransactions];

      $stateProvider
              .state('transaction', {
                 parent: 'root',
                 url: '/transaction',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('transaction.list', {
                 url: '/list',
                 reloadOnSearch: false,
                 templateUrl: '/transaction/list',
                 controller: 'TransactionListCtrl',
                 resolve: {
                    transactions: resolveTransactions,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('transaction/list');
                          return $translate.refresh();
                       }]
                 }
              });


      function loadTransactions($state, Transaction, logToServerUtil) {
         var transactionsPromise = Transaction.query().$promise;
         transactionsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Transactions failed', reason);
//                    $state.go('dashboard');
                 });
         return transactionsPromise;
      }

   }
})();
