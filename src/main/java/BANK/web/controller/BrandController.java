package BANK.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/brand")
public class BrandController {

    @RequestMapping("/list")
    public String list() {
        return "brand/list";
    }

    @RequestMapping("/common")
    public String common() {
        return "brand/common";
    }

    @RequestMapping("/add")
    public String add() {
        return "brand/add";
    }

    @RequestMapping("/edit")
    public String edit() {
        return "brand/edit";
    }

}
