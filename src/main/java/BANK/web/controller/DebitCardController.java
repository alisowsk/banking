package BANK.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/debitcard")
public class DebitCardController {

    @RequestMapping("/list")
    public String list() { return "debitcard/list"; }

    @RequestMapping("/common")
    public String common() { return "debitcard/common"; }

    @RequestMapping("/add")
    public String add() {
        return "debitcard/add";
    }

    @RequestMapping("/edit")
    public String edit() { return "debitcard/edit"; }

}
