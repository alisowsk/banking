package BANK.web.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyAccountController {

  @RequestMapping("/myaccount/list")
   public String list() {
      return "myaccount/list";
   }

   @RequestMapping("/myaccount/common")
   public String common() {
      return "myaccount/common";
   }

   @RequestMapping("/myaccount/details")
   public String detail() {
      return "myaccount/details";
   }
   
   @RequestMapping("/myaccount/add")
   public String add() {
      return "myaccount/add";
   }
   
   @RequestMapping("/myaccount/boost")
   public String boost() {
      return "myaccount/boost";
   }
   
   @RequestMapping("/myaccount/edit")
   public String edit() {
      return "myaccount/edit";
   }

}
