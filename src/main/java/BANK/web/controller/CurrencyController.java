package BANK.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CurrencyController {

    @RequestMapping("/currency/list")
    public String list() {
        return "currency/list";
    }

    @RequestMapping("/currency/common")
    public String common() {
        return "currency/common";
    }

    @RequestMapping("/currency/details")
    public String detail() {
        return "currency/details";
    }

    @RequestMapping("/currency/add")
    public String add() {
        return "currency/add";
    }

    @RequestMapping("/currency/edit")
    public String edit() {
        return "currency/edit";
    }

}
