package BANK.web.restapi.debitcard;

import BANK.domain.debitcard.bo.IDebitCardBO;
import BANK.domain.debitcard.dto.DebitCardSnapshot;
import BANK.domain.debitcard.finder.IDebitCardSnapshotFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping("/api/debitcard")
public class DebitCardApi {

    private final IDebitCardSnapshotFinder debitCardSnapshotFinder;
    private final IDebitCardBO debitCardBO;

    @Autowired
    public DebitCardApi(IDebitCardSnapshotFinder debitCardSnapshotFinder, IDebitCardBO debitCardBO) {
        this.debitCardSnapshotFinder = debitCardSnapshotFinder;
        this.debitCardBO = debitCardBO;
    }

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE)
    public HttpEntity<DebitCard> add(@RequestBody DebitCardNew debitCardNew) {

        DebitCardSnapshot debitCardSnapshot = debitCardBO.add(debitCardNew.getColor(), debitCardNew.getBrand(),
                debitCardNew.getDailyLimitTransactionValue(), debitCardNew.getDailyLimitTransactionNumber(),
                debitCardNew.getAccount(), debitCardNew.getCardStatus(),
                debitCardNew.getClient(), debitCardNew.getPan(), debitCardNew.getExpiryDate(),
                debitCardNew.getCvv(), debitCardNew.getPin());
        return getOKResponseEntity(debitCardSnapshot);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<DebitCard> list() {
        return debitCardSnapshotFinder.findAll().stream()
                .map(DebitCard::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET)
    public HttpEntity<DebitCard> get(@PathVariable("id") Long id) {
        DebitCardSnapshot debitCardSnapshot = debitCardSnapshotFinder.findById(id);
        if (debitCardSnapshot == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return getOKResponseEntity(debitCardSnapshot);
        }
    }

    @RequestMapping(value = "delete/{id}",
            method = RequestMethod.DELETE)
    public HttpEntity delete(@PathVariable("id") Long debitCardId) {
        DebitCardSnapshot debitCardSnapshot = debitCardSnapshotFinder.findById(
                debitCardId);
        if (debitCardId != null) {
            debitCardBO.delete(debitCardId);
        }
        return getOKResponseEntity(debitCardSnapshot);
    }

    @RequestMapping(value = "setPin/{id}", method = PUT,
            consumes = APPLICATION_JSON_VALUE)
    public HttpEntity<DebitCard> setDebitCardPin(
            @RequestBody DebitCardEdit debitCardEdit) {
        DebitCardSnapshot debitCardSnapshot = debitCardBO.setPin(debitCardEdit.getDebitCardId(),debitCardEdit.getPin());
        return getOKResponseEntity(debitCardSnapshot);
    }

    @RequestMapping(value = "setDailyLimitValue/{id}", method = PUT,
            consumes = APPLICATION_JSON_VALUE)
    public HttpEntity<DebitCard> setDebitCardDailyLimitValue(
            @RequestBody DebitCardEdit debitCardEdit) {
        DebitCardSnapshot debitCardSnapshot = debitCardBO.setDailyLimitValue(debitCardEdit.getDebitCardId(),debitCardEdit.getDailyLimitTransactionValue());
        return getOKResponseEntity(debitCardSnapshot);
    }

    @RequestMapping(value = "setDailyLimitTransactionNumber/{id}", method = PUT,
            consumes = APPLICATION_JSON_VALUE)
    public HttpEntity<DebitCard> setDebitCardDailyLimitTransactionNumber(
            @RequestBody DebitCardEdit debitCardEdit) {
        DebitCardSnapshot debitCardSnapshot = debitCardBO.setDailyLimitTransactionNumber(debitCardEdit.getDebitCardId(),debitCardEdit.getDailyLimitTransactionNumber());
        return getOKResponseEntity(debitCardSnapshot);
    }

    @RequestMapping(value = "suspend/{id}", method = PUT,
            consumes = APPLICATION_JSON_VALUE)
    public HttpEntity<DebitCard> suspendDebitCard(
            @RequestBody DebitCardEdit debitCardEdit) {
        DebitCardSnapshot debitCardSnapshot = debitCardBO.suspendDebitCard(debitCardEdit.getDebitCardId());
        return getOKResponseEntity(debitCardSnapshot);
    }

    @RequestMapping(value = "block/{id}", method = PUT,
            consumes = APPLICATION_JSON_VALUE)
    public HttpEntity<DebitCard> blockDebitCard(
            @RequestBody DebitCardEdit debitCardEdit) {
        DebitCardSnapshot debitCardSnapshot = debitCardBO.blockDebitCard(debitCardEdit.getDebitCardId());
        return getOKResponseEntity(debitCardSnapshot);
    }

    @RequestMapping(value = "{id}/withdraw/{amount}", method = PUT,
            consumes = APPLICATION_JSON_VALUE)
    public HttpEntity<DebitCard> withdraw(
            @PathVariable("id") Long debitCardId, @PathVariable("amount") Double amount) {
        return new ResponseEntity<> (debitCardBO.withdraw(debitCardId, amount));
    }

    @RequestMapping(value = "{id}/payintomoney/{amount}", method = PUT,
            consumes = APPLICATION_JSON_VALUE)
    public HttpEntity<DebitCard> payIntoMoney(
            @PathVariable("id") Long debitCardId, @PathVariable("amount") Double amount) {
        return new ResponseEntity<>(debitCardBO.payIntoMoney(debitCardId, amount));
    }

    private HttpEntity<DebitCard> getOKResponseEntity(DebitCardSnapshot debitCardSnapshot) {
        return new ResponseEntity<>(new DebitCard(debitCardSnapshot), HttpStatus.OK);
    }

}
