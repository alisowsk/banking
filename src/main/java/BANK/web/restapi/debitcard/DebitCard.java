package BANK.web.restapi.debitcard;

import BANK.domain.debitcard.dto.DebitCardSnapshot;
import org.springframework.hateoas.ResourceSupport;

import java.io.Serializable;
import java.time.LocalDateTime;

public class DebitCard extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = -950853360390856080L;

    private final Long cardId;

    private final String color;

    private final Long brand;

    private final Long dailyLimitTransactionValue;

    private final Long dailyLimitTransactionNumber;

    private final Long account;

    private final String cardStatus;

    private final Long client;

    private final int pan;

    private final LocalDateTime expiryDate;

    private final int cvv;

    private final int pin;

    private final Boolean isSuspended;

    private final Boolean isBlocked;

    public DebitCard(DebitCardSnapshot debitCardSnapshot) {
        this.cardId = debitCardSnapshot.getCardId();
        this.color = debitCardSnapshot.getColor();
        this.brand = debitCardSnapshot.getBrand();
        this.dailyLimitTransactionValue = debitCardSnapshot.getDailyLimitTransactionValue();
        this.dailyLimitTransactionNumber = debitCardSnapshot.getDailyLimitTransactionNumber();
        this.account = debitCardSnapshot.getAccount();
        this.cardStatus = debitCardSnapshot.getCardStatus();
        this.client = debitCardSnapshot.getClient();
        this.pan = debitCardSnapshot.getPan();
        this.expiryDate = debitCardSnapshot.getExpiryDate();
        this.cvv = debitCardSnapshot.getCvv();
        this.pin = debitCardSnapshot.getPin();
        this.isBlocked =debitCardSnapshot.isBlocked();
        this.isSuspended = debitCardSnapshot.isSuspended();
    }

    public Long getCardId() {
        return cardId;
    }

    public String getColor() {
        return color;
    }

    public Long getBrand() {
        return brand;
    }

    public Long getDailyLimitTransactionValue() {
        return dailyLimitTransactionValue;
    }

    public Long getDailyLimitTransactionNumber() {
        return dailyLimitTransactionNumber;
    }

    public Long getAccount() {
        return account;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public Long getClient() {
        return client;
    }

    public int getPan() {
        return pan;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public int getCvv() {
        return cvv;
    }

    public int getPin() {
        return pin;
    }

    public Boolean isSuspended() {
        return isSuspended;
    }

    public Boolean isBlocked() {
        return isBlocked;
    }
}
