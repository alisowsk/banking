package BANK.web.restapi.debitcard;

import BANK.web.converter.LocalDateTimeDeserializer;
import BANK.web.restapi.annotations.RangeOfTimestamp;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class DebitCardNew {

    @NotNull
    private String color;

    @NotNull
    //TODO join columns
    private Long brand;

    @NotNull
    private Long dailyLimitTransactionValue;

    @NotNull
    private Long dailyLimitTransactionNumber;

    @NotNull
    //TODO join columns
    private Long account;

    @NotNull
    private String cardStatus;

    @NotNull
    //TODO join columns
    private Long client;

    @NotNull
    private int pan;

    @NotNull
    @RangeOfTimestamp
    private LocalDateTime expiryDate;

    @NotNull
    private int cvv;

    @NotNull
    private int pin;

    @NotNull
    private Boolean isSuspended;

    @NotNull
    private Boolean isBlocked;

    public Boolean isSuspended() {
        return isSuspended;
    }

    public void suspend() {
        isSuspended = Boolean.TRUE;
    }

    public Boolean isBlocked() {
        return isBlocked;
    }

    public void block() {
        isBlocked = Boolean.TRUE;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getBrand() {
        return brand;
    }

    public void setBrand(Long brand) {
        this.brand = brand;
    }

    public Long getDailyLimitTransactionValue() {
        return dailyLimitTransactionValue;
    }

    public void setDailyLimitTransactionValue(Long dailyLimitTransactionValue) {
        this.dailyLimitTransactionValue = dailyLimitTransactionValue;
    }

    public Long getDailyLimitTransactionNumber() {
        return dailyLimitTransactionNumber;
    }

    public void setDailyLimitTransactionNumber(Long dailyLimitTransactionNumber) {
        this.dailyLimitTransactionNumber = dailyLimitTransactionNumber;
    }

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public Long getClient() {
        return client;
    }

    public void setClient(Long client) {
        this.client = client;
    }

    public int getPan() {
        return pan;
    }

    public void setPan(int pan) {
        this.pan = pan;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }
}
