package BANK.web.restapi.user;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import BANK.domain.user.dto.UserSnapshot;
import BANK.domain.user.finder.IUserSnapshotFinder;

import com.fasterxml.jackson.annotation.JsonView;

import BANK.domain.user.bo.IUserBO;

@RestController
@RequestMapping("/api/user")
public class UserApi {

   
   private final IUserSnapshotFinder userSnapshotFinder;

   private final IUserBO userBO;

   @Autowired
   public UserApi(IUserSnapshotFinder userSnapshotFinder, IUserBO userBO) {
      this.userSnapshotFinder = userSnapshotFinder;
      this.userBO = userBO;
   }


   @RequestMapping(method = RequestMethod.GET)
   public List<User> list(Principal principal) {
      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());
      List<UserSnapshot> userSnapshots;
      if(userSnapshot.getRole().toUpperCase().equals("ROLE_WORKER")){
         userSnapshots = userSnapshotFinder.findByRole("ROLE_USER");
      }else{
         userSnapshots = userSnapshotFinder.findAll();
      }


      return userSnapshots.stream()
         .map(User::new)
         .collect(Collectors.toList());
   }


   @RequestMapping(value = "/active",
      method = RequestMethod.GET)
   public List<User> active() {
      List<UserSnapshot> userSnapshots = userSnapshotFinder.findActive();

      return userSnapshots.stream()
         .map(User::new)
         .collect(Collectors.toList());
   }

   @RequestMapping(value = "/{id}",
      method = RequestMethod.GET)
   public HttpEntity<User> get(@PathVariable("id") long id) {
      UserSnapshot userSnapshot = userSnapshotFinder.findById(id);

      if (userSnapshot == null) {
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      } else {
         return new ResponseEntity<>(new User(userSnapshot), HttpStatus.OK);
      }
   }

   @RequestMapping(value = "/logged",
      method = RequestMethod.GET)
   public User getLoggedUser(Principal principal) {
      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());

      return new User(userSnapshot);

   }


   @RequestMapping(method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<User> add(@RequestBody UserNew userNew) {

      UserSnapshot userSnapshot = userBO.add(UUID.randomUUID(), userNew.getFirstname(),
         userNew.getLastname(),
         userNew.getRole(), userNew.getFirstname() + '.' + userNew.getLastname(),
         LocalDateTime.now(), userNew.getFirstname() + "." + userNew.getLastname() + "@bank.pl", "ROLE_"+userNew.getRole().toUpperCase(),
         "password");

      return new ResponseEntity<>(new User(userSnapshot), HttpStatus.OK);
   }


   @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
   @RequestMapping(method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<User> update(
      @RequestBody UserEdit userEdit) {

      UserSnapshot foundedUser = userSnapshotFinder.findById(userEdit.getUserId());

      UserSnapshot userSnapshot = userBO.edit(userEdit.getUserId(), foundedUser.getGuid(),
         userEdit.getFirstname(), userEdit.getLastname(),
         "User", userEdit.getFirstname() + '.' + userEdit.getLastname(),
         userEdit.getFirstname() + "." + userEdit.getLastname() + "@bank.pl");

      return new ResponseEntity<>(new User(userSnapshot), HttpStatus.OK);
   }


   @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
   @RequestMapping(value = "block/{id}",
      method = RequestMethod.DELETE)
   public HttpEntity<User> block(@PathVariable("id") Long userId) {
      UserSnapshot userSnapshot = userSnapshotFinder.findById(
         userId);
      if (userId != null && !userSnapshot.getRole().equals("ROLE_ADMIN")) {
         userBO.block(userId);
         return new ResponseEntity<>(new User(userSnapshot), HttpStatus.OK);
      }

      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
   }


   @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
   @RequestMapping(value = "unlock/{id}",
      method = RequestMethod.PUT)
   public HttpEntity<User> unlock(@PathVariable("id") Long userId) {
      UserSnapshot userSnapshot = userSnapshotFinder.findById(
         userId);
      if (userId != null) {
         userBO.unlock(userId);
      }

      return new ResponseEntity<>(new User(userSnapshot), HttpStatus.OK);
   }


   @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
   @RequestMapping(value = "delete/{id}",
      method = RequestMethod.DELETE)
   public HttpEntity delete(@PathVariable("id") Long userId) {
      UserSnapshot userSnapshot = userSnapshotFinder.findById(
         userId);
      if (userId != null) {
         userBO.delete(userId);
      }

      return new ResponseEntity<>(new User(userSnapshot), HttpStatus.OK);
   }


   @RequestMapping(value = "/external/block/{id}",
      method = RequestMethod.PUT)
   public HttpEntity extBlock(@PathVariable("id") Long userId) {
      UserSnapshot userSnapshot = userSnapshotFinder.findById(
         userId);

      if (userSnapshot != null) {
         userBO.unlock(userId);
         return new ResponseEntity(HttpStatus.OK);
      }

      return new ResponseEntity(HttpStatus.BAD_REQUEST);
   }

}
