package BANK.web.restapi.brand;

import javax.validation.constraints.NotNull;

public class BrandEdit {

    @NotNull
    private Long brandId;

    @NotNull
    private String brandName;

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
