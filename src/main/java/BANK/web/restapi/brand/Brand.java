package BANK.web.restapi.brand;

import BANK.domain.brand.dto.BrandSnapshot;
import org.springframework.hateoas.ResourceSupport;

import java.io.Serializable;

public class Brand extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = 6188152030887044513L;

    private final Long brandId;

    private final String brandName;

    public Brand(BrandSnapshot brandSnapshot) {
        this.brandId = brandSnapshot.getId();
        this.brandName = brandSnapshot.getBrandName();
    }

    public Long getBrandId() {
        return brandId;
    }

    public String getBrandName() {
        return brandName;
    }
}
