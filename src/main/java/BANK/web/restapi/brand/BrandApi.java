package BANK.web.restapi.brand;

import BANK.domain.brand.bo.IBrandBO;
import BANK.domain.brand.dto.BrandSnapshot;
import BANK.domain.brand.finder.IBrandSnapshotFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/api/brand")
public class BrandApi {

    @Autowired
    private IBrandSnapshotFinder brandSnapshotFinder;

    @Autowired
    private IBrandBO brandBO;

    @RequestMapping(method = GET)
    public List<Brand> list() {
        List<BrandSnapshot> brandSnapshots = brandSnapshotFinder.findAll();
        return brandSnapshots.stream()
                .map(Brand::new)
                .collect(toList());
    }

    @RequestMapping(value = "/{id}", method = GET)
    public HttpEntity<Brand> get(@PathVariable("id") long id) {
        BrandSnapshot brandSnapshot = brandSnapshotFinder.findById(id);
        return brandSnapshot == null
                ? new ResponseEntity<>(NOT_FOUND) : responseOk(brandSnapshot);
    }

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE)
    public HttpEntity<Brand> add(@RequestBody BrandNew brandNew) {
        BrandSnapshot brandSnapshot = brandBO.add(brandNew.getBrandName());
        return responseOk(brandSnapshot);
    }

    @RequestMapping(method = PUT, consumes = APPLICATION_JSON_VALUE)
    public HttpEntity<Brand> update(@RequestBody BrandEdit brandEdit) {
        BrandSnapshot brandSnapshot = brandBO.edit(brandEdit.getBrandId(), brandEdit.getBrandName());
        return responseOk(brandSnapshot);
    }

    @RequestMapping(value = "delete/{id}", method = DELETE)
    public HttpEntity delete(@PathVariable("id") long id) {
        BrandSnapshot brandSnapshot = brandSnapshotFinder.findById(id);
        ofNullable(brandSnapshot).ifPresent($ -> brandBO.delete(id));
        return responseOk(brandSnapshot);
    }

    private ResponseEntity<Brand> responseOk(BrandSnapshot brandSnapshot) {
        return new ResponseEntity<>(new Brand(brandSnapshot), OK);
    }

}
