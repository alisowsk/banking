package BANK.web.restapi.brand;

import javax.validation.constraints.NotNull;

public class BrandNew {

    @NotNull
    private String brandName;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

}