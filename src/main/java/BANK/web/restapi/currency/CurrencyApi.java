package BANK.web.restapi.currency;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import BANK.domain.currency.dto.CurrencySnapshot;
import BANK.domain.currency.finder.ICurrencySnapshotFinder;

import com.fasterxml.jackson.annotation.JsonView;

import BANK.domain.currency.bo.ICurrencyBO;

@RestController
@RequestMapping("/api/currency")
public class CurrencyApi {

   
   private final ICurrencySnapshotFinder currencySnapshotFinder;

   private final ICurrencyBO currencyBO;

   @Autowired
   public CurrencyApi(ICurrencySnapshotFinder currencySnapshotFinder, ICurrencyBO currencyBO) {
      this.currencySnapshotFinder = currencySnapshotFinder;
      this.currencyBO = currencyBO;
   }


   @RequestMapping(method = RequestMethod.GET)
   public List<Currency> list() {
      List<CurrencySnapshot> currencySnapshots = currencySnapshotFinder.findAll();

      return currencySnapshots.stream()
         .map(Currency::new)
         .collect(Collectors.toList());
   }

   @RequestMapping(value = "/{id}",
      method = RequestMethod.GET)
   public HttpEntity<Currency> get(@PathVariable("id") long id) {
      CurrencySnapshot currencySnapshot = currencySnapshotFinder.findById(id);

      if (currencySnapshot == null) {
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      } else {
         return new ResponseEntity<>(new Currency(currencySnapshot), HttpStatus.OK);
      }
   }


   @RequestMapping(method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<Currency> add(@RequestBody CurrencyNew currencyNew) {
         
      CurrencySnapshot currencySnapshot = currencyBO.add(currencyNew.getSymbol());

      return new ResponseEntity<>(new Currency(currencySnapshot), HttpStatus.OK);
   }


   @RequestMapping(method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<Currency> update(
      @RequestBody CurrencyEdit currencyEdit) {
      CurrencySnapshot currencySnapshot = currencyBO.edit(currencyEdit.getCurrencyId(),currencyEdit.getSymbol());

      return new ResponseEntity<>(new Currency(currencySnapshot), HttpStatus.OK);
   }

   @RequestMapping(value = "delete/{id}",
      method = RequestMethod.DELETE)
   public HttpEntity delete(@PathVariable("id") Long currencyId) {
      CurrencySnapshot currencySnapshot = currencySnapshotFinder.findById(
         currencyId);
      
      if (currencyId != null) {
         currencyBO.delete(currencyId);
      }

      return new ResponseEntity<>(new Currency(currencySnapshot), HttpStatus.OK);
   }
}
