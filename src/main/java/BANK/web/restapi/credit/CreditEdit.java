/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.credit;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;



/**
 *
 * @author Mateusz
 */
public class CreditEdit {
   
   @NotNull
   private Long creditId;
   
   @NotNull
   private Long provision;
   
   @NotNull
   private Long interest;

   @NotNull
   private Long installment;
   
   @NotNull
   private Double contribution;
   
   @NotNull
   private Double minValue;
   
   @NotNull
   private Double maxValue;
   
   @NotNull
   private String type;

   private boolean changingRate;
   
   private boolean insurance;
   
   @NotNull
   private Long currencyId;

   public Long getCreditId() {
      return creditId;
   }

   public void setCreditId(Long creditId) {
      this.creditId = creditId;
   }

   public Long getProvision() {
      return provision;
   }

   public void setProvision(Long provision) {
      this.provision = provision;
   }

   public Long getInterest() {
      return interest;
   }

   public void setInterest(Long interest) {
      this.interest = interest;
   }

   public Long getInstallment() {
      return installment;
   }

   public void setInstallment(Long installment) {
      this.installment = installment;
   }

   public Double getContribution() {
      return contribution;
   }

   public void setContribution(Double contribution) {
      this.contribution = contribution;
   }

   public boolean isChangingRate() {
      return changingRate;
   }

   public void setChangingRate(boolean changingRate) {
      this.changingRate = changingRate;
   }

   public boolean isInsurance() {
      return insurance;
   }

   public void setInsurance(boolean insurance) {
      this.insurance = insurance;
   }

   public Double getMinValue() {
      return minValue;
   }

   public void setMinValue(Double minValue) {
      this.minValue = minValue;
   }

   public Double getMaxValue() {
      return maxValue;
   }

   public void setMaxValue(Double maxValue) {
      this.maxValue = maxValue;
   }

   public String getType() {
      return type;
   }

   public void setType(String type) {
      this.type = type;
   }

   public Long getCurrencyId() {
      return currencyId;
   }

   public void setCurrencyId(Long currencyId) {
      this.currencyId = currencyId;
   }
}
