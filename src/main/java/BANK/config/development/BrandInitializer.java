package BANK.config.development;

import BANK.domain.brand.bo.IBrandBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Stream;

import static BANK.sharedkernel.constant.Profiles.DEVELOPMENT;

@Component
@Profile(DEVELOPMENT)
public class BrandInitializer implements IBrandInitializer {

    @Autowired
    private IBrandBO brandBO;

    @Transactional
    @Override
    public void initialize() {

        Stream.of("MasterCard", "Visa", "Discover", "American Express")
                .forEach($ -> brandBO.add($));

    }

}
