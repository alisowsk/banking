package BANK.config.development;

import BANK.domain.user.bo.IUserBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static BANK.sharedkernel.constant.Profiles.DEVELOPMENT;
import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;

@Component
@Profile(DEVELOPMENT)
public class UserInitializer implements IUserInitializer {

    @Autowired
    private IUserBO userBO;

    @Transactional
    @Override
    public void initialize() {
        addUser("Mariusz", "Kłysiński", "User", "mariusz.klysinski", "mariusz.klysinski@bank.pl", "ROLE_USER", "password");
        addUser("Andrzej", "Lisowski", "User", "andrzej.lisowski", "andrzej.lisowski@bank.pl", "ROLE_USER", "password");

        addUser("Mateusz", "Głąbicki", "Worker", "mateusz.glabicki", "mateusz.glabicki@bank.pl", "ROLE_WORKER", "password");
        addUser("Szymon", "Łyszkowski", "Worker", "szymon.lyszkowski", "szymon.lyszkowski@bank.pl", "ROLE_WORKER", "password");

        addUser("Mateusz", "Łędzewicz", "Admin", "mateusz.ledzewicz", "mateusz.łędzewicz@bank.pl", "ROLE_ADMIN", "password");
        addUser("Agnieszka", "Kluska", "Admin", "agnieszka.kluska", "agnieszka.kluska@bank.pl", "ROLE_ADMIN", "password");
    }

    private void addUser(String name, String surname, String title, String username, String email, String role, String password) {
        userBO.add(randomUUID(), name, surname, title, username, now(), email, role, password);
    }

}
