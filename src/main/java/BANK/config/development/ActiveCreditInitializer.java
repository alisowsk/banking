package BANK.config.development;

import BANK.domain.credit.bo.IActiveCreditBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static BANK.sharedkernel.constant.Profiles.DEVELOPMENT;


@Component
@Profile(DEVELOPMENT)
public class ActiveCreditInitializer implements IActiveCreditInitializer {

    @Autowired
    private IActiveCreditBO activeCreditBO;

    @Transactional
    @Override
    public void initialize() {
        //1
        activeCreditBO.add(00001L, 1L, LocalDateTime.of(2016, 12, 30, 12, 00), 100.00);
        activeCreditBO.add(00002L, 1L, LocalDateTime.of(2017, 10, 30, 12, 00), 1000.00);
        activeCreditBO.add(00003L, 1L, LocalDateTime.of(2017, 4, 30, 12, 00), 10000.00);

        //2
        activeCreditBO.add(00004L, 2L, LocalDateTime.of(2018, 1, 30, 12, 00), 200.00);
        activeCreditBO.add(00005L, 2L, LocalDateTime.of(2017, 3, 30, 12, 00), 2000.00);
        activeCreditBO.add(00006L, 2L, LocalDateTime.of(2017, 8, 30, 12, 00), 20000.00);

        //3
        activeCreditBO.add(00007L, 3L, LocalDateTime.of(2018, 12, 30, 12, 00), 300.00);
        activeCreditBO.add(00010L, 3L, LocalDateTime.of(2017, 5, 30, 12, 00), 3000.00);
        activeCreditBO.add(00011L, 3L, LocalDateTime.of(2019, 1, 30, 12, 00), 30000.00);

    }

}
