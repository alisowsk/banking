package BANK.config.development;

import BANK.domain.credit.bo.ICreditBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static BANK.sharedkernel.constant.Profiles.DEVELOPMENT;


@Component
@Profile(DEVELOPMENT)
public class CreditInitializer implements ICreditInitializer {

    @Autowired
    private ICreditBO creditBO;

    @Transactional
    @Override
    public void initialize() {

        //1
        creditBO.add(2L, 10L, 1L, 1000.00, false, true, true, 0.00, 10000.00, "house", 3L);
        creditBO.add(1L, 15L, 1L, 1500.00, true, false, true, 0.00, 15000.00, "car", 1L);
        creditBO.add(4L, 5L, 1L, 10000.00, true, true, false, 0.00, 500000.00, "house", 2L);
        creditBO.add(3L, 7L, 1L, 5000.00, false, true, false, 0.00, 25000.00, "car", 3L);
        creditBO.add(2L, 20L, 1L, 100.00, false, false, true, 0.00, 1000.00, "study", 1L);
        creditBO.add(1L, 50L, 1L, 1000.00, true, false, true, 0.00, 50000.00, "house", 2L);

        //2
        creditBO.add(2L, 20L, 1L, 5000.00, true, false, true, 0.00, 10000.00, "house", 3L);
        creditBO.add(6L, 10L, 1L, 1000.00, false, true, true, 0.00, 5000.00, "car", 2L);
        creditBO.add(7L, 5L, 1L, 10000.00, false, true, false, 0.00, 100000.00, "car", 3L);
        creditBO.add(5L, 2L, 1L, 7000.00, true, true, true, 0.00, 230000.00, "study", 1L);
        creditBO.add(4L, 1L, 1L, 15000.00, true, false, false, 0.00, 100000.00, "money", 1L);
        creditBO.add(3L, 32L, 1L, 3000.00, false, true, false, 0.00, 15000.00, "money", 2L);

        //3
        creditBO.add(7L, 35L, 1L, 1000.00, false, true, true, 0.00, 10000.00, "car", 1L);
        creditBO.add(8L, 30L, 1L, 15000.00, false, true, false, 0.00, 150000.00, "car", 3L);
        creditBO.add(4L, 15L, 1L, 500.00, true, false, false, 0.00, 5000.00, "house", 2L);
        creditBO.add(3L, 10L, 1L, 1000.00, true, true, true, 0.00, 7000.00, "money", 3L);
        creditBO.add(2L, 5L, 1L, 500.00, true, false, true, 0.00, 2500.00, "money", 1L);
        creditBO.add(1L, 1L, 1L, 10000.00, false, false, true, 0.00, 160000.00, "money", 2L);
    }

}
