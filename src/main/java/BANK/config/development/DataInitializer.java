package BANK.config.development;

import BANK.domain.account.finder.IAccountSnapshotFinder;
import BANK.domain.brand.finder.IBrandSnapshotFinder;
import BANK.domain.credit.finder.IActiveCreditSnapshotFinder;
import BANK.domain.credit.finder.ICreditSnapshotFinder;
import BANK.domain.currency.finder.ICurrencySnapshotFinder;
import BANK.domain.debitcard.finder.IDebitCardSnapshotFinder;
import BANK.domain.user.finder.IUserSnapshotFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

import static BANK.sharedkernel.constant.Profiles.DEVELOPMENT;

@Component
@Profile(DEVELOPMENT)
public class DataInitializer {

    @Autowired
    private IUserInitializer userInitializer;

    @Autowired
    private IUserSnapshotFinder userSnapshotFinder;

    @Autowired
    private ICurrencyInitializer currencyInitializer;

    @Autowired
    private ICurrencySnapshotFinder currencySnapshotFinder;

    @Autowired
    private IActiveCreditInitializer activeCreditInitializer;

    @Autowired
    private IBrandInitializer brandInitializer;

    @Autowired
    private ICreditInitializer creditInitializer;

    @Autowired
    private IDebitCardInitializer debitCardInitializer;

    @Autowired
    private IActiveCreditSnapshotFinder activeCreditSnapshotFinder;

    @Autowired
    private ICreditSnapshotFinder creditSnapshotFinder;

    @Autowired
    private IAccountInitializer accountInitializer;

    @Autowired
    private IAccountSnapshotFinder accountSnapshotFinder;

    @Autowired
    private IBrandSnapshotFinder brandSnapshotFinder;

    @Autowired
    private IDebitCardSnapshotFinder debitCardSnapshotFinder;

    @Transactional
    @PostConstruct
    public void init() {
        //TODO extract common interface in Finder and do the init via Stream.of -> filter -> forEach

        if (userSnapshotFinder.findAll().isEmpty()) {
            userInitializer.initialize();
        }

        if (currencySnapshotFinder.findAll().isEmpty()) {
            currencyInitializer.initialize();
        }

        if (accountSnapshotFinder.findAll().isEmpty()) {
            accountInitializer.initialize();
        }

        if (creditSnapshotFinder.findAll().isEmpty()) {
            creditInitializer.initialize();
        }

        if (activeCreditSnapshotFinder.findAll().isEmpty()) {
            activeCreditInitializer.initialize();
        }

        if (brandSnapshotFinder.findAll().isEmpty()) {
            brandInitializer.initialize();
        }

        if(debitCardSnapshotFinder.findAll().isEmpty()){
            debitCardInitializer.initialize();
        }

    }

}
