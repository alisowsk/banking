package BANK.config.development;

import BANK.domain.account.bo.IAccountBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static BANK.sharedkernel.constant.Profiles.DEVELOPMENT;


@Component
@Profile(DEVELOPMENT)
public class AccountInitializer implements IAccountInitializer {

    @Autowired
    private IAccountBO accountBO;

    @Transactional
    @Override
    public void initialize() {
        //1
        accountBO.add("PL 91 0202 0202 0000 0000 0000 0001", 200000.00, 3L, true, 1L);
        accountBO.add("PL 91 0202 0202 0000 0000 0000 0002", 150000.00, 2L, false, 1L);
        accountBO.add("PL 91 0202 0202 0000 0000 0000 0003", 152000.00, 2L, false, 1L);

        //2
        accountBO.add("PL 91 0202 0202 0000 0000 0000 1201", 200000.00, 3L, false, 2L);
        accountBO.add("PL 91 0202 0202 0000 0000 0000 1202", 151000.00, 2L, true, 2L);
        accountBO.add("PL 91 0202 0202 0000 0000 0000 1203", 152000.00, 2L, false, 2L);

        //3
        accountBO.add("PL 91 0202 0202 0000 0000 0000 3001", 318000.00, 3L, true, 3L);
        accountBO.add("PL 91 0202 0202 0000 0000 0000 3002", 50000.00, 2L, false, 3L);
        accountBO.add("PL 91 0202 0202 0000 0000 0000 3003", 87000.00, 2L, false, 3L);

    }

}
