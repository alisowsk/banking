package BANK.config.development;

import BANK.domain.currency.bo.ICurrencyBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Stream;

import static BANK.sharedkernel.constant.Profiles.DEVELOPMENT;

@Component
@Profile(DEVELOPMENT)
public class CurrencyInitializer implements ICurrencyInitializer {

    @Autowired
    private ICurrencyBO currencyBO;

    @Transactional
    @Override
    public void initialize() {

        Stream.of("PLN", "EUR", "USD", "GBP", "CHF")
                .forEach($ -> currencyBO.add($));

    }

}
