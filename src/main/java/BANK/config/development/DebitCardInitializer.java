package BANK.config.development;

import BANK.domain.debitcard.bo.IDebitCardBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static BANK.sharedkernel.constant.Profiles.DEVELOPMENT;
import static java.time.LocalDateTime.of;

@Component
@Profile(DEVELOPMENT)
public class DebitCardInitializer implements IDebitCardInitializer {

    @Autowired
    private IDebitCardBO debitCardBO;

    @Transactional
    @Override
    public void initialize() {

        debitCardBO.add("RED", 1L, 4000L, 10L, 1L, "active", 1L, 402400, of(2018, 10, 10, 0, 0), 123, 1234);
        debitCardBO.add("GREEN", 1L, 2000L, 50L, 1L, "active", 1L, 436536, of(2017, 5, 2, 0, 0), 234, 7585);
        debitCardBO.add("BLUE", 2L, 10000L, 15L, 2L, "active", 2L, 768665, of(2019, 3, 3, 0, 0), 345, 6547);
        debitCardBO.add("RED", 3L, 500L, 3L, 3L, "active", 3L, 354754, of(2019, 4, 10, 0, 0), 456, 2345);

    }

}
