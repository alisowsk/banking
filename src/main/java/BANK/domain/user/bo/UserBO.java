package BANK.domain.user.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

import BANK.domain.user.dto.UserSnapshot;
import BANK.domain.user.entity.User;
import BANK.domain.user.exception.UserNotExistsException;
import BANK.domain.user.repo.IUserRepository;
import BANK.sharedkernel.annotations.BussinesObject;

/**
 *
 * @author Mateusz.Glabicki
 */
@BussinesObject
public class UserBO
   implements IUserBO {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserBO.class);
   private final IUserRepository userRepository;
   private final AuthenticationManagerBuilder auth;
   
   @Autowired
   public UserBO(IUserRepository userRepository, AuthenticationManagerBuilder auth) {
      this.userRepository = userRepository;
      this.auth = auth;
   }

   @Override
   public UserSnapshot add(UUID guid, String name, String lastname, String title, String username,
      LocalDateTime createdAt, String mail,String role,String password) {
      User user = new User(guid, title, name, lastname, username, createdAt, mail,role,password);

      user = userRepository.save(user);
      
      UserSnapshot userSnapshot = user.toSnapshot();

      LOGGER.info("Add User <{}> <{}> <{} {}> <{}> <{}> <{}>",
         userSnapshot.getId(),
         userSnapshot.getGuid(), userSnapshot.getLastname(), userSnapshot.getFirstname(),
         userSnapshot.getTitle(), userSnapshot.getUsername(), userSnapshot.getMail());

      return userSnapshot;
   }

   @Override
   public UserSnapshot edit(long id, UUID guid, String name, String lastname, String title, String username,
      String mail) {

      User user = userRepository.findOne(id);

      user.editUser(guid, name, lastname, title, username, mail);

      userRepository.save(user);

      UserSnapshot userSnapshot = user.toSnapshot();

      LOGGER.info("Edit User <{}> <{}> <{}> <{}> <{}> <{}>",
         userSnapshot.getId(), userSnapshot.getLastname(), userSnapshot.getFirstname(),
         userSnapshot.getTitle(), userSnapshot.getUsername(), userSnapshot.getMail());
      return userSnapshot;
   }

   @Override
   public void block(Long userId) {
      User user = userRepository.findOne(userId);
      user.block();
      userRepository.save(user);

      UserSnapshot userSnapshot = user.toSnapshot();
      
      LOGGER.info("User <{}> <{}> marked as blocked",
         userId, userSnapshot.getUsername());
   }
   
   @Override
   public void newPassword(Long userId,String password) {
      User user = userRepository.findOne(userId);
      user.newPassword(password);
      userRepository.save(user);

      UserSnapshot userSnapshot = user.toSnapshot();
      
      LOGGER.info("User <{}> <{}> change password",
         userId, userSnapshot.getUsername());
   }

   @Override
   public UserSnapshot unlock(Long userId) {
      User user = userRepository.findOne(userId);
      user.unlock();
      user = userRepository.save(user);

      LOGGER.info("User <{}> signed as userd", userId);

      return user.toSnapshot();
   }

   @Override
   public void delete(Long userId) {
      User user = userRepository.findOne(userId);
      userRepository.delete(user);

      LOGGER.info("User <{}> marked as deleted", userId);
   }
}
