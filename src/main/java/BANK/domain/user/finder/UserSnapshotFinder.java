package BANK.domain.user.finder;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import BANK.domain.user.dto.UserSnapshot;
import BANK.domain.user.entity.User;
import BANK.domain.user.repo.IUserRepository;
import BANK.sharedkernel.annotations.Finder;

/**
 *
 * @author Mateusz.Glabicki
 */
@Finder
public class UserSnapshotFinder
   implements IUserSnapshotFinder {

    private final IUserRepository userRepository;

   private static final Sort DEFAULT_SORT = new Sort("lastname", "firstname");

   @Autowired
   public UserSnapshotFinder(IUserRepository userRepository) {
      this.userRepository = userRepository;
   }

   private List<UserSnapshot> convert(List<User> users) {
      return users.stream()
         .map(User::toSnapshot)
         .collect(Collectors.toList());
   }

   @Override
   public UserSnapshot findById(Long id) {
      User user = userRepository.findOne(id);
      return user == null ? null : user.toSnapshot();
   }
   
   @Override
   public UserSnapshot findOneByUsernameAndPassword(String name,String password) {
      User user = userRepository.findOneByUsernameAndPassword(name,password);
      return user == null ? null : user.toSnapshot();
   }

   @Override
   public List<UserSnapshot> findAll() {
      List<User> users = userRepository.findAll(DEFAULT_SORT);

      return convert(users);
   }
   
   @Override
   public List<UserSnapshot> findByRole(String role) {
      List<User> users = userRepository.findByRole(role);

      return convert(users);
   }

   @Override
   public UserSnapshot findByUuid(UUID guid) {
      List<User> userAll = userRepository.findAll();
      List<User> users = userAll.stream()
         .filter(user -> user.toSnapshot().getGuid().toString().equals(guid.toString()))
         .collect(Collectors.toList());

      return users.isEmpty() ? null : users.get(0).toSnapshot();
   }

   @Override
   public UserSnapshot findByUsername(String username) {
      List<User> users = userRepository.findByUsernameIgnoreCase(username);

      return users.isEmpty() ? null : users.get(0).toSnapshot();

   }

   @Override
   public List<UserSnapshot> findAll(Set<Long> ids) {
      List<User> users = userRepository.findAll(ids);

      return convert(users);
   }

   @Override
   public List<UserSnapshot> findActive() {
      return convert(userRepository.findByEnabledTrue());
   }
   
   @Override
   public Map<Long, UserSnapshot> findAllAsMap(Set<Long> ids) {
      List<User> users = userRepository.findAll(ids);

      return users
            .stream()
            .map(User::toSnapshot)
            .collect(Collectors.toMap(UserSnapshot::getId, s ->s));
   }

}
