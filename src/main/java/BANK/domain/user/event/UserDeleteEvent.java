package BANK.domain.user.event;

import org.springframework.context.ApplicationEvent;

import BANK.domain.user.dto.UserSnapshot;

public class UserDeleteEvent
   extends ApplicationEvent {

   private static final long serialVersionUID = -2864077050151556923L;

   private final UserSnapshot userSnapshot;

   public UserDeleteEvent(UserSnapshot userSnapshot) {
      super(userSnapshot);
      this.userSnapshot = userSnapshot;
   }

   public UserSnapshot getUserSnapshot() {
      return userSnapshot;
   }
}
