package BANK.domain.credit.finder;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import BANK.domain.credit.dto.ActiveCreditSnapshot;
import BANK.domain.credit.dto.CreditSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IActiveCreditSnapshotFinder {

   ActiveCreditSnapshot findById(Long id);

   List<ActiveCreditSnapshot> findAll();
   
   List<ActiveCreditSnapshot> findByUserId(Long userId);

   List<ActiveCreditSnapshot> findAll(Set<Long> ids);

   List<ActiveCreditSnapshot> findActive();
   
   List<ActiveCreditSnapshot> findByCreditId(Long id);
   

}
