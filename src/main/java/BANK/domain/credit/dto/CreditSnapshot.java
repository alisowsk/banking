package BANK.domain.credit.dto;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Mateusz.Glabicki
 */
public class CreditSnapshot {

   private final Long id;

   private final LocalDateTime createdAt;
   
   private final long version;
   
   private final Long provision;
   
   private final Long interest;

   private final Long installment;
   
   private final Double contribution;
   
   private final Double minValue;
   
   private final Double maxValue;

   private final boolean changingRate;
   
   private final boolean active;
 
   private final boolean insurance;
   
   private final String type;
   
   private final Long currencyId;

   public CreditSnapshot(Long id, LocalDateTime createdAt, long version, Long provision, Long interest, Long installment,
      Double contribution, boolean changingRate, boolean active, boolean insurance, Double minValue, Double maxValue, String type, Long currencyId) {
      this.id = id;
      this.createdAt = createdAt;
      this.version = version;
      this.provision = provision;
      this.interest = interest;
      this.installment = installment;
      this.contribution = contribution;
      this.changingRate = changingRate;
      this.active = active;
      this.insurance = insurance;
      this.minValue = minValue;
      this.maxValue = maxValue;
      this.type = type;
      this.currencyId = currencyId;
   }

   public Double getMinValue() {
      return minValue;
   }

   public Double getMaxValue() {
      return maxValue;
   }

   public Long getId() {
      return id;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }

   public long getVersion() {
      return version;
   }

   public Long getProvision() {
      return provision;
   }

   public Long getInterest() {
      return interest;
   }

   public Long getInstallment() {
      return installment;
   }

   public Double getContribution() {
      return contribution;
   }

   public boolean isChangingRate() {
      return changingRate;
   }

   public boolean isActive() {
      return active;
   }

   public boolean isInsurance() {
      return insurance;
   }

   public String getType() {
      return type;
   }

   public Long getCurrencyId() {
      return currencyId;
   }
   
   @Override
   public boolean equals(Object obj) {
      if (!(obj instanceof CreditSnapshot)) {
         return false;
      }
      CreditSnapshot emp = (CreditSnapshot) obj;
      return this.getId().equals(emp.getId());
   }

   @Override
   public int hashCode() {
      int hash = 7;
      hash = 37 * hash + Objects.hashCode(this.id);
      return hash;
   }

}
