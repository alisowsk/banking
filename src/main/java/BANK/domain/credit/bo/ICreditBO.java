package BANK.domain.credit.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import BANK.domain.credit.dto.CreditSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ICreditBO {

   CreditSnapshot add(Long provision, Long interest, Long installment, Double contribution, boolean changingRate,
      boolean active, boolean insurance,Double minValue, Double maxValue, String type, Long currencyId);

   CreditSnapshot edit(Long id, Long provision, Long interest, Long installment, Double contribution, boolean changingRate,
       boolean insurance,Double minValue, Double maxValue, String type, Long currencyId);

   void delete(Long creditId);

   void finish(Long creditId);
}
