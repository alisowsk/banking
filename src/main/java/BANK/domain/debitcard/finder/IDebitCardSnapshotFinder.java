package BANK.domain.debitcard.finder;

import BANK.domain.debitcard.dto.DebitCardSnapshot;

import java.util.List;

/**
 * Created by lyszkows on 13/01/2017.
 */
public interface IDebitCardSnapshotFinder {

    List<DebitCardSnapshot> findAll();

    DebitCardSnapshot findById(Long id);

}
