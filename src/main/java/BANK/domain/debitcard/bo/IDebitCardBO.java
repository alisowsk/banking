package BANK.domain.debitcard.bo;

import BANK.domain.debitcard.dto.DebitCardSnapshot;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public interface IDebitCardBO {

    DebitCardSnapshot add(String color, Long brand, Long dailyLimitTransactionValue, Long dailyLimitTransactionNumber,
                          Long account, String cardStatus, Long client, int pan,
                          LocalDateTime expiryDate, int cvv, int pin);

    void delete(Long debitCardId);

    DebitCardSnapshot setPin(Long id, Integer newPin);

    DebitCardSnapshot setDailyLimitValue(Long debitCardId, Long dailyLimitTransactionValue);

    DebitCardSnapshot setDailyLimitTransactionNumber(Long debitCardId, Long dailyLimitTransactionValue);

    DebitCardSnapshot suspendDebitCard(Long debitCardId);

    DebitCardSnapshot blockDebitCard(Long debitCardId);

    HttpStatus withdraw(Long debitCardId, Double amount);

    HttpStatus payIntoMoney(Long debitCardId, Double amount);
}
