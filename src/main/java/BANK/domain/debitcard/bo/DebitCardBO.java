package BANK.domain.debitcard.bo;

import BANK.domain.account.dto.AccountSnapshot;
import BANK.domain.account.entity.Account;
import BANK.domain.account.repo.IAccountRepository;
import BANK.domain.debitcard.dto.DebitCardSnapshot;
import BANK.domain.debitcard.entity.DebitCard;
import BANK.domain.debitcard.repo.IDebitCardRepository;
import BANK.domain.user.repo.IUserRepository;
import BANK.sharedkernel.annotations.BussinesObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@BussinesObject
public class DebitCardBO implements IDebitCardBO {

    private static final Logger LOGGER = LoggerFactory.getLogger(DebitCardBO.class);

    @Autowired
    private IAccountRepository accountRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IDebitCardRepository debitCardRepository;

    @Override
    public DebitCardSnapshot add(String color, Long brand, Long dailyLimitTransactionValue,
                                 Long dailyLimitTransactionNumber, Long account,
                                 String cardStatus, Long client, int pan, LocalDateTime expiryDate, int cvv, int pin) {
        DebitCard debitCard = new DebitCard(color, brand, dailyLimitTransactionValue, dailyLimitTransactionNumber, account,
                cardStatus, client, pan, expiryDate, cvv, pin);

        debitCard = debitCardRepository.save(debitCard);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        LOGGER.info("Add debit card <{}>", debitCardSnapshot.getCardId());
        return debitCardSnapshot;
    }

    @Override
    public void delete(Long debitCardId) {
        final DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        debitCardRepository.delete(debitCard);
        LOGGER.info("Deleted debit card <{}>", debitCardId);
    }


    @Override
    public DebitCardSnapshot setPin(Long id, Integer newPin) {
        DebitCard debitCard = debitCardRepository.findOne(id);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        debitCard.setPin(newPin);
        debitCardRepository.save(debitCard);
        LOGGER.info("Debit card with id <{}> PIN number set to <{}>",
                debitCardSnapshot.getCardId(), debitCardSnapshot.getPin());
        return debitCardSnapshot;
    }

    @Override
    public DebitCardSnapshot setDailyLimitValue(Long debitCardId, Long dailyLimitTransactionValue) {
        DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        debitCard.setDailyLimitTransactionValue(dailyLimitTransactionValue);
        debitCardRepository.save(debitCard);
        LOGGER.info("Debit card with id <{}> daily transaction value set to <{}>",
                debitCardSnapshot.getCardId(), debitCardSnapshot.getDailyLimitTransactionValue());
        return debitCardSnapshot;
    }

    @Override
    public DebitCardSnapshot setDailyLimitTransactionNumber(Long debitCardId, Long dailyLimitTransactionNumber) {
        DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        debitCard.setDailyLimitTransactionNumber(dailyLimitTransactionNumber);
        debitCardRepository.save(debitCard);
        LOGGER.info("Debit card with id <{}> daily transaction number set to <{}>",
                debitCardSnapshot.getCardId(), debitCardSnapshot.getDailyLimitTransactionNumber());
        return debitCardSnapshot;
    }

    @Override
    public DebitCardSnapshot suspendDebitCard(Long debitCardId) {
        DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        debitCard.suspend();
        debitCardRepository.save(debitCard);
        LOGGER.info("Debit card with id <{}> suspended",
                debitCardSnapshot.getCardId(), debitCardSnapshot.isSuspended());
        return debitCardSnapshot;
    }

    @Override
    public DebitCardSnapshot blockDebitCard(Long debitCardId) {
        DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        DebitCardSnapshot debitCardSnapshot = debitCard.toSnapshot();
        debitCard.block();
        debitCardRepository.save(debitCard);
        LOGGER.info("Debit card with id <{}> blocked",
                debitCardSnapshot.getCardId(), debitCardSnapshot.isBlocked());
        return debitCardSnapshot;
    }

    @Override
    public HttpStatus withdraw(Long debitCardId, Double amount) {
        DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        Long accountId = debitCard.getAccountId();
        Account accountToWithdraw = accountRepository.getOne(accountId);
        AccountSnapshot accountSnapshot = accountToWithdraw.toAccountSnapshot();
        double result = accountSnapshot.getBalance() - amount;
        if (result >= 0) {
            editAccount(accountToWithdraw, accountSnapshot, result);
            return HttpStatus.OK;
        }
        return HttpStatus.NOT_ACCEPTABLE;
    }

    @Override
    public HttpStatus payIntoMoney(Long debitCardId, Double amount) {
        DebitCard debitCard = debitCardRepository.findOne(debitCardId);
        Long accountId = debitCard.getAccountId();
        Account accountToWithdraw = accountRepository.getOne(accountId);
        AccountSnapshot accountSnapshot = accountToWithdraw.toAccountSnapshot();
        double result = accountSnapshot.getBalance() + amount;
        editAccount(accountToWithdraw, accountSnapshot, result);
        return HttpStatus.OK;
    }

    private void editAccount(Account accountToWithdraw, AccountSnapshot accountSnapshot, double result) {
        accountToWithdraw.editAccount(accountSnapshot.getIban(), result, accountSnapshot.getCurrencyId());
        accountRepository.save(accountToWithdraw);
    }

}
