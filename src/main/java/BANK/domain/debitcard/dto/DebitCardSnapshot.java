package BANK.domain.debitcard.dto;

import java.time.LocalDateTime;

public class DebitCardSnapshot {

    private final Long cardId;

    private final String color;

    private final Long brand;

    private final Long dailyLimitTransactionValue;

    private final Long dailyLimitTransactionNumber;

    private final Long account;

    private final String cardStatus;

    private final Long client;

    private final int pan;

    private final LocalDateTime expiryDate;

    private final int cvv;

    private final int pin;

    private final Boolean isSuspended;

    private final Boolean isBlocked;

    public DebitCardSnapshot(Long cardId, String color, Long brand, Long dailyLimitTransactionValue,
                             Long dailyLimitTransactionNumber, Long account,
                             String cardStatus, Long client, int pan, LocalDateTime expiryDate, int cvv, int pin) {
        this.cardId = cardId;
        this.color = color;
        this.brand = brand;
        this.dailyLimitTransactionValue = dailyLimitTransactionValue;
        this.dailyLimitTransactionNumber = dailyLimitTransactionNumber;
        this.account = account;
        this.cardStatus = cardStatus;
        this.client = client;
        this.pan = pan;
        this.expiryDate = expiryDate;
        this.cvv = cvv;
        this.pin = pin;
        this.isBlocked = Boolean.FALSE;
        this.isSuspended = Boolean.FALSE;
    }

    public Long getCardId() {
        return cardId;
    }

    public String getColor() {
        return color;
    }

    public Long getBrand() {
        return brand;
    }

    public Long getDailyLimitTransactionValue() {
        return dailyLimitTransactionValue;
    }

    public Long getDailyLimitTransactionNumber() {
        return dailyLimitTransactionNumber;
    }

    public Long getAccount() {
        return account;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public Long getClient() {
        return client;
    }

    public int getPan() {
        return pan;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public int getCvv() {
        return cvv;
    }

    public int getPin() {
        return pin;
    }

    public Boolean isSuspended() {
        return isSuspended;
    }

    public Boolean isBlocked() {
        return isBlocked;
    }
}
