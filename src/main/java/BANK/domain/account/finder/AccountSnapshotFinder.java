package BANK.domain.account.finder;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import BANK.domain.account.dto.AccountSnapshot;
import BANK.domain.account.entity.Account;
import BANK.domain.account.repo.IAccountRepository;
import BANK.sharedkernel.annotations.Finder;

/**
 *
 * @author Mateusz.Glabicki
 */
@Finder
public class AccountSnapshotFinder
   implements IAccountSnapshotFinder {

   private final IAccountRepository accountRepository;

   @Autowired
   public AccountSnapshotFinder(IAccountRepository accountRepository) {
      this.accountRepository = accountRepository;
   }

   private List<AccountSnapshot> convert(List<Account> accounts) {
      return accounts.stream()
         .map(Account::toAccountSnapshot)
         .collect(Collectors.toList());
   }

   @Override
   public AccountSnapshot findById(Long id) {
      Account account = accountRepository.findOne(id);
      return account == null ? null : account.toAccountSnapshot();
   }


   @Override
   public AccountSnapshot findByIban(String iban) {
      List<Account> accounts = accountRepository.findByIban(iban);

      return accounts.isEmpty() ? null : accounts.get(0).toAccountSnapshot();
   }

   @Override
   public List<AccountSnapshot> findAll() {
      List<Account> accounts = accountRepository.findAll();

      return convert(accounts);
   }
   
   @Override
   public List<AccountSnapshot> findByUserId(Long id) {
      List<Account> accounts = accountRepository.findByUserId(id);

      return convert(accounts);
   }
   
   @Override
   public List<AccountSnapshot> findByActiveTrue() {
      List<Account> accounts = accountRepository.findByActiveTrue();

      return convert(accounts);
   }
   
   @Override
   public List<AccountSnapshot> findByActiveTrueAndUserId(Long userId) {
      List<Account> accounts = accountRepository.findByActiveTrueAndUserId(userId);

      return convert(accounts);
   }

}
