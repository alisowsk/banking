package BANK.domain.account.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.account.dto.AccountSnapshot;
import BANK.web.converter.LocalDateTimeDeserializer;
import BANK.sharedkernel.exception.EntityInStateNewException;

/**
 *
 * @author Mateusz.Glabicki
 */
@Entity
public class Account
   implements Serializable {

   private static final long serialVersionUID = -1355310491554028529L;

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long id;

   @NotNull
   private Long currencyId;

   @NotEmpty
   @Size(min = 1,
      max = 50)
   @Column(nullable = false,
      unique = true)
   private String iban;

   @NotNull
   private Double balance;

   @NotNull
   private boolean active;

   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime createdAt;

   @NotNull
   private Long userId;

   protected Account() {
   }

   public Account(String iban, Double balance, Long currencyId, boolean active, Long userId) {
      this.createdAt = LocalDateTime.now();
      this.iban = iban;
      this.balance = balance;
      this.currencyId = currencyId;
      this.active = active;
      this.userId = userId;
   }

   public void active() {
      this.active = true;
   }

   public void boost(Double balance) {
      this.balance += balance;
      BigDecimal modelVal = new BigDecimal(this.balance);
      BigDecimal val = modelVal.setScale(2, RoundingMode.HALF_EVEN);
      this.balance = val.doubleValue();
   }

   public void disable() {
      this.active = false;
   }

   public void editAccount(String iban, Double balance, Long currencyId) {
      this.iban = iban;
      this.balance = balance;
      this.currencyId = currencyId;
   }

   public AccountSnapshot toAccountSnapshot() {
      if (id == null) {
         throw new EntityInStateNewException();
      }
      return new AccountSnapshot(id, iban, balance, createdAt, currencyId, active, userId);
   }

}
