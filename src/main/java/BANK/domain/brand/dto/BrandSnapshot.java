package BANK.domain.brand.dto;

public class BrandSnapshot {

    private final Long id;

    private final String brandName;

    public BrandSnapshot(Long id, String brandName) {
        this.id = id;
        this.brandName = brandName;
    }

    public Long getId() {
        return id;
    }

    public String getBrandName() {
        return brandName;
    }

}
