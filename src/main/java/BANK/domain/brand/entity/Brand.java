package BANK.domain.brand.entity;

import BANK.domain.brand.dto.BrandSnapshot;
import BANK.sharedkernel.exception.EntityInStateNewException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class Brand implements Serializable {

    private static final long serialVersionUID = 6088580414522799229L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String brandName;

    public Brand() {
    }

    public Brand(String brandName) {
        this.brandName = brandName;
    }

    public void editBrand(String brandName) {
        this.brandName = brandName;
    }

    public BrandSnapshot toBrandSnapshot() {
        if (id == null) {
            throw new EntityInStateNewException();
        }
        return new BrandSnapshot(id, brandName);
    }
}
