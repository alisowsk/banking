package BANK.domain.brand.repo;

import BANK.domain.brand.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IBrandRepository extends JpaRepository<Brand, Long> {

    List<Brand> findByBrandName(String brandName);

}
