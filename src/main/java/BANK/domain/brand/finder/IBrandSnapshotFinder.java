package BANK.domain.brand.finder;

import BANK.domain.brand.dto.BrandSnapshot;

import java.util.List;
import java.util.Set;

public interface IBrandSnapshotFinder {

    BrandSnapshot findById(Long id);

    List<BrandSnapshot> findAll(Set<Long> ids);

    List<BrandSnapshot> findAll();

}