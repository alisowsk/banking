package BANK.domain.brand.finder;

import BANK.domain.brand.dto.BrandSnapshot;
import BANK.domain.brand.entity.Brand;
import BANK.domain.brand.repo.IBrandRepository;
import BANK.sharedkernel.annotations.Finder;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@Finder
public class BrandSnapshotFinder implements IBrandSnapshotFinder {

    @Autowired
    private IBrandRepository brandRepository;

    @Override
    public BrandSnapshot findById(Long id) {
        Brand brand = brandRepository.findOne(id);
        return brand == null ? null : brand.toBrandSnapshot();
    }

    @Override
    public List<BrandSnapshot> findAll() {
        List<Brand> brands = brandRepository.findAll();
        return convert(brands);
    }
//
//    @Override
//    public BrandSnapshot findByBrandName(String brandName) {
//        List<Brand> brands = brandRepository.findByBrandName(brandName);
//        return Optional.of(brands)
//                .orElse(null)
//                .get(0).toBrandSnapshot();
//    }

    @Override
    public List<BrandSnapshot> findAll(Set<Long> ids) {
        List<Brand> brands = brandRepository.findAll(ids);
        return convert(brands);
    }

    private List<BrandSnapshot> convert(List<Brand> brands) {
        return brands.stream()
                .map(Brand::toBrandSnapshot)
                .collect(toList());
    }
}