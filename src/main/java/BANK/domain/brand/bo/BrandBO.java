package BANK.domain.brand.bo;

import BANK.domain.brand.dto.BrandSnapshot;
import BANK.domain.brand.entity.Brand;
import BANK.domain.brand.repo.IBrandRepository;
import BANK.sharedkernel.annotations.BussinesObject;
import org.springframework.beans.factory.annotation.Autowired;

@BussinesObject
public class BrandBO implements IBrandBO {

    @Autowired
    private IBrandRepository brandRepository;

    @Override
    public BrandSnapshot add(String name) {
        Brand brand = new Brand(name);
        brand = brandRepository.save(brand);

        return brand.toBrandSnapshot();
    }

    @Override
    public void delete(Long id) {
        brandRepository.delete(id);
    }

    @Override
    public BrandSnapshot edit(Long id, String symbol) {
        Brand brand = brandRepository.findOne(id);

        brand.editBrand(symbol);
        brandRepository.save(brand);

        return brand.toBrandSnapshot();
    }

}
