package BANK.domain.brand.bo;

import BANK.domain.brand.dto.BrandSnapshot;

public interface IBrandBO {

    BrandSnapshot add(String name);

    void delete(Long id);

    BrandSnapshot edit(Long id, String name);

}
