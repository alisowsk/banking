package BANK.domain.currency.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

import BANK.domain.currency.dto.CurrencySnapshot;
import BANK.domain.currency.entity.Currency;
import BANK.domain.currency.repo.ICurrencyRepository;
import BANK.sharedkernel.annotations.BussinesObject;

@BussinesObject
public class CurrencyBO
   implements ICurrencyBO {

   private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyBO.class);

   private final ICurrencyRepository currencyRepository;


   @Autowired
   public CurrencyBO(ICurrencyRepository currencyRepository) {
      this.currencyRepository = currencyRepository;
   }


   @Override
   public CurrencySnapshot add(String symbol) {
      Currency currency = new Currency(symbol);
      currency = currencyRepository.save(currency);

      CurrencySnapshot currencySnapshot = currency.toCurrencySnapshot();

      LOGGER.info("Add Currency <{}> <{}>",
         currencySnapshot.getId(), currencySnapshot.getSymbol());

      return currencySnapshot;
   }

   @Override
   public void delete(Long id) {
      Currency currency = currencyRepository.findOne(id);
      currencyRepository.delete(id);

      LOGGER.info("Delete Currency <{}>", id);
   }

   @Override
   public CurrencySnapshot edit(Long id, String symbol) {
      Currency currency = currencyRepository.findOne(id);

      CurrencySnapshot currencySnapshot = currency.toCurrencySnapshot();

      currency.editCurrency(symbol);
      currencyRepository.save(currency);

      LOGGER.info("Edit Currency <{}> <{}>",
         currencySnapshot.getId(), currencySnapshot.getSymbol());
      
      return currencySnapshot;
   }

}
