package BANK.domain.currency.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import BANK.domain.currency.entity.Currency;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ICurrencyRepository
   extends JpaRepository<Currency, Long> {

   List<Currency> findBySymbol(String symbol);
}
