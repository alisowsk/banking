package BANK.domain.installment.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

import BANK.domain.installment.dto.InstallmentSnapshot;
import BANK.domain.installment.entity.Installment;
import BANK.domain.installment.repo.IInstallmentRepository;
import BANK.sharedkernel.annotations.BussinesObject;

/**
 *
 * @author Mateusz.Glabicki
 */
@BussinesObject
public class InstallmentBO
   implements IInstallmentBO {

   private static final Logger LOGGER = LoggerFactory.getLogger(InstallmentBO.class);
   private final IInstallmentRepository installmentRepository;

   
   @Autowired
   public InstallmentBO(IInstallmentRepository installmentRepository) {
      this.installmentRepository = installmentRepository;
   }

   @Override
   public InstallmentSnapshot add(Double amount,LocalDateTime finishDate, Long activeCreditId) {
      Installment installment = new Installment(amount,finishDate,activeCreditId);

      installment = installmentRepository.save(installment);
      
      InstallmentSnapshot installmentSnapshot = installment.toSnapshot();

      LOGGER.info("Add Installment <{}>", installmentSnapshot.getId());

      return installmentSnapshot;
   }


//   @Override
//   public void delete(Long installmentId) {
//      Installment installment = installmentRepository.findOne(installmentId);
//      installmentRepository.delete(installment);
//
//      LOGGER.info("Installment <{}> marked as deleted", installmentId);
//   }
   
   @Override
   public void pay(Long installmentId) {
      Installment installment = installmentRepository.findOne(installmentId);
      installment.pay();
      installmentRepository.save(installment);

      InstallmentSnapshot installmentSnapshot = installment.toSnapshot();
      
      LOGGER.info("Installment <{}>  marked as finished",   installmentId);
   }
}
